import prompt from "prompt";
class Game {
  constructor(length = 4) {
    this.secret = Game.secretNumber(length);
    this.length = length;
  }

  static secretNumber = (length) => {
    let secret;
    const max = 10 ** length - 1;
    const min = 10 ** (length - 1);
    do {
      secret = Math.floor(Math.random() * max);
    } while (Game.checkDistinctNumber(secret) || secret < min);

    return secret;
  };

  static checkDistinctNumber(secret) {
    const str = secret + "";
    const test = [];
    test.push(str[0]);
    for (let i = 1; i < str.length; i++) {
      if (test.includes(str[i])) {
        return true;
      } else {
        test.push(str[i]);
      }
    }

    return false;
  }
  static askForNumber = async () => {
    const { attemp } = await prompt.get(["attemp"]);
    return attemp;
  };

  static checkAttemp = (attemp, secret) => {
    const strAttemp = attemp + "";
    const strSecret = secret + "";
    let cows = 0;
    let bulls = 0;

    for (let i = 0; i < strSecret.length; i++) {
      if (strSecret.includes(strAttemp[i])) {
        strSecret[i] === strAttemp[i] ? bulls++ : cows++;
      }
    }
    return { bulls, cows };
  };

  async star() {
    try {
      console.log("Game start:");
      for (let index = 0; index < this.length * 2; index++) {
        console.log("please give a number:");
        const attemp = await Game.askForNumber();
        const result = Game.checkAttemp(attemp, this.secret);
        console.log(result);
        if (result.bulls === this.length) {
          console.log("Well done, you win 🏆🏆🏆");
          return;
        }
      }
      console.log("you lose 💣💣💣");
      console.log("the secret number is:", this.secret);
    } catch (err) {
      console.log(err.message);
    }
  }
}

export default Game;
